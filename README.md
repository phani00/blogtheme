# blogtheme theme

developing a theme from scratch, NOT forked from boet or redbasic.

meant for a personal blog site.

to install it go to the hub's webroot and issue the following command:

<code>util/add_theme_repo https://github.com/phani00/blogtheme.git plusbasic</code>
